<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dnka' );

/** MySQL database username */
define( 'DB_USER', 'wp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'u<+QzU+vh!OW_MbEtU@:0[dkX5sw6.Wzym>j_/WZ2cQtt,4Zs={SZ_X 2fd+.pmp' );
define( 'SECURE_AUTH_KEY',  'M&F0m+;#qxcU~/ pQa!G|h&q5DVDL2zxDzpXyGqjTeYR)z(eiQR30j*}Y/*@=~<|' );
define( 'LOGGED_IN_KEY',    'h&2]RRZ6PRU`sf$>Yf$l&>(g4,NAhk.7kP).)>,rtUj]++X8kDAO3[V)TA&gnq@}' );
define( 'NONCE_KEY',        ')9!48go@$pZS{|k{PwItGEhmkOpZ{YV2Bq{cP>2aw?.b2xK!ZV[drv|uA.|e!I0,' );
define( 'AUTH_SALT',        'SzG{?K(FmOQ;JmW~5^pT+1m1RP@};ne@~dJ~#(Imib#KDA3@R$Qp[CT*~oL%ka>_' );
define( 'SECURE_AUTH_SALT', ',Cc?,9>,l5dg]$5DP,%7ZE;`I^N`V@8_jT(zhly6iN#l?G 5(0a0Dg-&THPyyTpD' );
define( 'LOGGED_IN_SALT',   'cc<XlV<rA mMd+2h^Y^U7#Cb^#r &qO*6^eMR31%+13;{:VK1CSab-~6UZqck@wi' );
define( 'NONCE_SALT',       '%vr0bBTu+Mbc68 5yFhFXSSe~+,u{2 eF6<j2ya7ukb<(tT>~X(:+0JGNb<!k07z' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'WP_DEBUG', true );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
