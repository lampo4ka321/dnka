<?php
/**
 * The blog page.
 *
 * @package Neat
 */

get_header(); ?>

<?php
/**
 * Homepage
 */
 
// Context array.
$context = array();
 
// Add the page ID which is 4 in my case.
$context[ 'welcome_page' ] = Timber::get_post( 1803 );

// Timber render().
Timber::render( 'welcome.twig', $context );

?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
