(function ($){
    $( document ).ready(function() {
        var $body = $('body'),
            $burger = $('.js-burger'),
            $menu = $('.js-header-drop');

        $burger.on('click', function (e) {
            e.preventDefault();
            var $this = $burger.find('.header__burger-box');

            if ($this.hasClass('header__burger--open')) {
                $this.removeClass('header__burger--open');
                $menu.removeClass('header__drop--active');
                $body.removeClass('is-opened');
            } else {
                $this.addClass('header__burger--open');
                $menu.addClass('header__drop--active');
                $body.addClass('is-opened');
            }
        });
    });
})(jQuery);